Rails.application.routes.draw do
  get 'product_available_areas/index'

  get 'user_accounts/show'

  get 'order/index'

  get 'admin/index'
  get 'product/autocomplete_product_name'

  # get 'cart/index'

  # get 'admin_panel/new'

  # get 'admin_panel/show'

  # get 'admin_panel/edit'

  get 'home/index'
devise_for :users, controllers: { registrations: "registration" }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  resources :product do
    resource :cart_items
      member do
        put :add_category
        put :check_product_for_city
        get :product_avaliability
      end
  end  

  resources :product do
    member do
      post :products_by_category
      post :create_update_related_product
      get :count_additional_images
      get :get_additional_images
    end
  resources :order
  end

resources :order do
  member do
    get :gen_order_invoice
  end
end

resources :cart do
  member do
    put :quantity_update
  end
end
  resources :admin
  resources :user_accounts
  resource :reviews
  resource :products_likes
  resources :shipping_addresses do
    member do
      put :update_selected_address
    end
  end
  
  resources :locations
  resources :pictures 
  resources :hello
  resources :conversations do
    member do
      get :notify_typing_action
    end
    resources :messages
  end

  resources :account_activations, only: [:edit]
  
  root 'product#index'
  # get 'addproduct'   => 'admin_panel#new'
  # get 'allproduct'   => 'admin_panel#index'
  # get 'product' => 'admin_panel#show'
  # get 'editproduct'   => 'admin_panel#edit'
  # post 'product'  => 'admin_panel#create'
  post 'product_search' => 'product#search'
  get 'admin_orders_status' => 'order#admin_orders_status'
  post 'admin_order_search' => 'order#search'
  post 'add_product_to_cart' => 'product#addtocart'
  get  'cart_index' => 'cart#index'
  get 'about' => 'home#about'
  get 'contact' => 'home#contact'
  get 'help' => 'home#help'
  get 'product_filter' => 'product#filter'
  get 'account_close' => 'home#accountclose'
  put 'update_order_status' => 'order#update_order_status'
  put 'remove_image_frm_dir' => 'pictures#remove_image_frm_dir'
  #post 'product_quantity_update' => 'cart#quantity_update' 
  # patch 'updateproduct' => 'admin_panel#update'

  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  get "*path" =>  redirect('/users/sign_up') #new_user_registration_path #("/")

end
