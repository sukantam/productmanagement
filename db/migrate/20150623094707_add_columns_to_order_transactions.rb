class AddColumnsToOrderTransactions < ActiveRecord::Migration
  def change
    add_column :order_transactions, :payment_id, :string
    add_column :order_transactions, :transaction_id, :string
    add_column :order_transactions, :mode, :string
  end
end
