class CreateProductAndAvailableareaJoinTable < ActiveRecord::Migration
  def change
  	create_table :locations_products, id: false do |t|
  		t.belongs_to :product, index: true, foregin_key: true
  		t.belongs_to :location, index: true, foregin_key: true
  	end
  end
end
