class CreateProductLikes < ActiveRecord::Migration
  def change
    create_table :product_likes do |t|
      t.integer :like
      t.references :user, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
