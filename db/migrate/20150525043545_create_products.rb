class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :product_name
      t.text :product_desc
      t.string :product_image
      t.boolean :isVisible_to_user

      t.timestamps null: false
    end
  end
end
