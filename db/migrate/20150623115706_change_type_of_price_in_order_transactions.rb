class ChangeTypeOfPriceInOrderTransactions < ActiveRecord::Migration
  def up
    change_column :order_transactions, :amount, :decimal,:precision => 18, :scale => 2
  end

  def down
    change_column :order_transactions, :amount, :decimal,:precision => 10, :scale => 0
  end
end
