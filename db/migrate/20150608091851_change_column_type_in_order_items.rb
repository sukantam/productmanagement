class ChangeColumnTypeInOrderItems < ActiveRecord::Migration
  def up
    change_column :order_items, :total_price, :decimal,:precision => 18, :scale => 2
  end

  def down
    change_column :order_items, :total_price, :decimal,:precision => 10, :scale => 0
  end
end
