class ChangeTypeOfColumnInOrders < ActiveRecord::Migration
  def up
    change_column :orders, :delivery_place, :text
  end

  def down
    change_column :orders, :delivery_place, :integer
  end
end
