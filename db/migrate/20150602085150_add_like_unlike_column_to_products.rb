class AddLikeUnlikeColumnToProducts < ActiveRecord::Migration
  def change
    add_column :products, :likes, :integer
    add_column :products, :unlikes, :integer
  end
end
