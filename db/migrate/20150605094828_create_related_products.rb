class CreateRelatedProducts < ActiveRecord::Migration
  def change
    create_table :related_products do |t|
      t.references :product, index: true, foreign_key: true
      t.string :related_products

      t.timestamps null: false
    end
  end
end
