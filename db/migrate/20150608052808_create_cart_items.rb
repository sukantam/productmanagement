class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.references :product, index: true, foreign_key: true
      t.string :quantity

      t.timestamps null: false
    end
  end
end
