class AddColumnToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :expected_devlivery_on, :date
    add_column :orders, :delivery_on, :date
    add_column :orders, :status, :string
  end
end
