class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fullname, :string
    add_column :users, :phone, :string
    add_column :users, :street_line1, :string
    add_column :users, :street_line2, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :country, :string
    add_column :users, :pin, :string
  end
end
