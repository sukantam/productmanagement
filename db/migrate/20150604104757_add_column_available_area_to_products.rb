class AddColumnAvailableAreaToProducts < ActiveRecord::Migration
  def change
    add_column :products, :area_to_available, :text
  end
end
