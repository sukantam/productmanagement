class ChangeCartItemsType < ActiveRecord::Migration
  def up
    change_column :cart_items, :quantity, :integer
  end
   def down
    change_column :cart_items, :quantity, :string
  end
end
