class CreateShippingAddresses < ActiveRecord::Migration
  def change
    create_table :shipping_addresses do |t|
      t.references :user , index: true, foregin_key: true
      t.text :address_to_ship
      t.boolean :isSelected

      t.timestamps null: false
    end
  end
end
