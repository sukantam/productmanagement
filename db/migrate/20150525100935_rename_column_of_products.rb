class RenameColumnOfProducts < ActiveRecord::Migration
  def change
  	rename_column :products, :product_name, :name
  	rename_column :products, :product_desc, :description
  	rename_column :products, :product_image, :image
  	rename_column :products, :products_type_id, :product_type_id

  	rename_column :products_types, :type_name, :name
  end
end
