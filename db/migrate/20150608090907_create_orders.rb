class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :paid_price, :precision => 18, :scale => 2
      t.integer :quantity
      t.text	:delivery_place
      t.timestamps null: false
    end
  end
end
