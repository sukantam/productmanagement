class ChangeColumnTypeOfRelatedProductsInRelatedProducts < ActiveRecord::Migration
  def up
    change_column :related_products, :related_products, :text
  end

  def down
    change_column :related_products, :related_products, :string
  end
end
