class UserAccountsController < ApplicationController
  before_action :authenticate_user!
  def show
  	if ( current_user.isAdmin || (current_user.id == params[:id].to_i))
  		@user = User.find(params[:id])
  	else
  		flash[:warning] = "Access denied! as you are trying to access wrong user"
  		@user = User.find(current_user.id)
  		redirect_to user_account_path(@user)
  	end

  end

  def destroy
  	@user = User.find(params[:id]).destroy
	flash[:success] = "Your account is succesfully closed."
	sign_out
	redirect_to account_close_path
  end

  private

end
