class ShippingAddressesController < ApplicationController
	before_action :authenticate_user!

	def create
		ShippingAddress.created_new_address(current_user, params[:address])
		render json: {:status => "success"}
	end
	
	def update_selected_address
		ShippingAddress.update_selected_address(current_user, params[:id])
		render json: {:id => params[:id]}
	end

	def destroy
		ShippingAddress.remove(current_user,params[:id])
		render json: {:id => params[:id]}
	end
end
