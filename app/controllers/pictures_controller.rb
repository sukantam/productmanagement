class PicturesController < ApplicationController

	def create
		
		if params[:product_id].present?

			product = Product.find(params[:product_id])

			if params[:picture][:image].present?

				uploaded_to = params[:picture][:image]

				directory = GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + product.id.to_s

				if(!directory_exists?(directory))

					create_directory(directory)
				end


				prv_images_count = Dir[File.join(directory, "**", "*")].count{ |file| File.file?(file) }

				if save_additional_images(directory, uploaded_to, prv_images_count)
					
					flash[:success] = "Added successfully"
					redirect_to product

				else
					render json: {:status => GlobalConstants::FAILURE }
				end

			end

		else

		end

	end

	def remove_image_frm_dir

		file_path = GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + params[:product_id].to_s + "/" + params[:image]

		if File.exists?(file_path)
			File.delete(file_path)
			render json: { status: GlobalConstants::SUCCESS, product_id: params[:product_id], image: params[:image]}
		else
			render json: { status: GlobalConstants::FAILURE}
		end

	end

	private

	def directory_exists?(directory)
		
		Dir.exists?(directory)

	end

	def create_directory(directory)

		require 'fileutils'

		FileUtils.mkdir_p(directory)

	end

	def save_additional_images(directory, uploaded_to, prv_images_count)

		
		#file open
    	File.open(Rails.root.join(directory, prv_images_count.to_s + File.extname(uploaded_to.original_filename)), 'wb') do |file|
    
    	#write coverted bytes to file
    	file.write(uploaded_to.read)

    	end
    	
    	rescue
    		logger.error($!.to_s)

	end

end
