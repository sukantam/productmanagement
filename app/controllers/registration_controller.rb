class RegistrationController < Devise::RegistrationsController

	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found_wrong_access

def create

	build_resource(registration_params)
	uploaded_io = params[:user][:image]

	#create the new user
	@user = User.new(registration_params)

	#checking for image existance
	if !uploaded_io.nil?

		last_id =1
		#save image file
		if User.last.present?
  			save_image(User.last.id,'users',uploaded_io, Rails.application.config.image_path)
  			last_id =User.last.id + 1
  		else
  			save_image(1,'users',uploaded_io, Rails.application.config.image_path)
  			last_id = 1
  		end

		@user.image = last_id.to_s + "_" + uploaded_io.original_filename
	else
		@user.image = "user-default.png"
  	end
	
  	#save user details to database table
	if @user.save	

		Picture.create_new(@user)
			
		#create cart for user
		Cart.create_cart(@user.id)

		#create default shipping address
		ShippingAddress.create_ship_addr(@user)

		sign_in(@user)
		
		flash[:success] = "your account is created"
		redirect_to product_index_path
	else			
  		flash.now[:danger] = "Failed to save, try again"
		clean_up_passwords resource
     	 @validatable = devise_mapping.validatable?
      if @validatable
       	 @minimum_password_length = resource_class.password_length.min
      end
      	respond_with resource
	end

end
 
def destroy
end

def update

	if (!resource.image.nil? && 
			(File.exist?(Rails.root.join(
				Rails.application.config.image_path, 'users', resource.image))))
      File.delete(Rails.root.join(Rails.application.config.image_path, 'users', resource.image))
end
	if resource.update_attributes(registration_update_params)
		if( !params[:user][:image].nil? )

        uploaded_io = params[:user][:image]

        #save image
    	save_image(Product.last.id,'users',uploaded_io,Rails.application.config.image_path)	
    	#update user account details
    	resource.update_attribute(:image, (Product.last.id+1).to_s+"_"+uploaded_io.original_filename)

  	end
 	flash[:success] = "Your account is updated"
 	redirect_to user_account_path(resource)
 else
 	respond_with resource
end
end

private

#save images
def save_image(last_id, folder_to_save,uploaded_io,image_path)
	 File.open(Rails.root.join(image_path, folder_to_save, (last_id+1).to_s+"_"+uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
    end
end
#parameters for create account
def registration_params
	params.require(:user).permit(:email,:image, :fullname, :street_line1, :street_line2, 
  	:city, :state, :country, :phone, :pin, :password, :password_confirmation)
	end

	#parameters for update account
def registration_update_params
	params.require(:user).permit(:fullname, :street_line1, :street_line2, 
  	:city, :state, :country, :phone, :pin)
end


	def check_password_change_permission?
		if(!params[:user][:current_passowrd].blank?)
			user = User.find_by(email: params[:user][:email].downcase)

			if user && user.authenticate(params[:user][:current_passowrd])
				return true
			else
				flash.now[:danger] ="Enter correct current passowrd"
				respond_with resource			
			end
		else
			return true
		end
	end
end
