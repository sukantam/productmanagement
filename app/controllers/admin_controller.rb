class AdminController < ApplicationController

	authorize_resource :class=>false

  def index
  	@users = User.all.order("created_at desc")
  end
	
end
