class OrderController < ApplicationController

  before_action :authenticate_user!, only: [:create, :index]
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found_wrong_access
  
  #action to return all orders placed by current logged in user to index view page
  def index
  	@orders = Order.get_all_placed_orders(current_user)
    @order_ids = current_user.orders.order("created_at desc")
  end

# create order using cart id
  def create
    begin    
      @list_items = hash_of_items(params)
      #credit card information
      @credit_card = credit_card(params)
      @response = Order.pay_now(params, current_user, @list_items, @credit_card)
      
      if @response.id.present?
          order = Order.create_order(current_user)
          order_item =nil
        	if(params[:product_id].nil? || params[:product_id].to_i==0)

      	  	cart_item_ids = params[:ids]
            amount_paid = 0;
            total_items = 0;

            if(order.present?)          
              #for sigle or multiple order placed through cart section
        	  	cart_item_ids.each do |c|
        	  		#find cart details
        	  		@cart_item = CartItem.find(c)
                if product_avaliability(@cart_item.quantity,@cart_item.product_id) 
                  
            	  		# call methode to createe order
                    order_item = OrderItem.create_order_item(@cart_item, order)

            	  		if order_item.id.present?

                      amount_paid = amount_paid + (@cart_item.quantity * (Product.find(@cart_item.product_id)).price)
                      total_items =total_items + 1
            	  		  # remove cart item
            	  		  @cart_item.destroy

                    else

                      selected_address = current_user.shipping_addresses.where(isSelected: true).first
                      order.update_attributes(:paid_price => amount_paid, :quantity => total_items,
                                      :delivery_place => selected_address.address_to_ship)
                      #create transaction record after successfull payment
                      OrderTransaction.create_transaction(@response, order.id, amount_paid)
                      flash[:info] = "Their might be some problem. please try again"
                      redirect_to cart_index_path
                    end
                end
              end
            end
      	  else
            #for order placed from home or product details page
      	  	product = Product.find(params[:product_id])
      	  	@cart_item = CartItem.new(product_id: params[:product_id],
        							             quantity: params[:quantity])         

            #check wether user specified quantity available or not
      	  	if (product_avaliability(params[:quantity], params[:product_id]))              

               order_item = OrderItem.create_order_item(@cart_item, order)
               selected_address = current_user.shipping_addresses.where(isSelected: true).first
               order.update_attributes(:paid_price => product.price, :quantity => params[:quantity],
                                        :delivery_place => selected_address.address_to_ship)

               #create transaction record after successfull payment
              OrderTransaction.create_transaction(@response, order.id, product.price)

               respond_to do |format|
                 msg = { :status => "ok", :message => "Success!",:product_id => params[:product_id] }
                       format.json  { render :json => msg } 
                end
              return
            else
              order.destroy
              respond_to do |format|
                 msg = { :status => "ok", :message => "failed",:product_id => params[:product_id] }
                       format.json  { render :json => msg } 
            end
              return
      	    end
          end          
          selected_address = current_user.shipping_addresses.where(isSelected: true).first
          if(total_items>0)
              order.update_attributes(:paid_price => amount_paid, :quantity => total_items,
                                      :delivery_place => selected_address.address_to_ship)
              #create transaction record after successfull payment
              if OrderTransaction.create_transaction(@response, order.id, amount_paid)
              	flash[:success] = "your orders has been placed"
              	redirect_to order_index_path
              end
          else
              order.destroy
              flash[:danger] = "Found some problem in placing order"
              redirect_to cart_index_path
          end
    else
      # failed transaction
      flash[:error] = "Gatway Server problem"
    end
    rescue
      if order
          order.destroy
      end  
      flash[:warning] = "Error #{$!} or Gatway Server connection problem"
      redirect_to cart_index_path
    ensure               
    end
  end

  def update_order_status

    Order.update_status(params)
    render json: { order_id: params[:order_id], status: "success"}

  end

  def admin_orders_status

     option = Order.statuses[:pending]

     if params[:search].present? && params[:filter].present?

          @order_ids = Order.get_orders_id(params) 

          @orders = Order.get_all_matched_orders(@order_ids)

     else
        if params[:filter].present?
            option = params[:filter].to_i

            if option == Order.statuses[:pending]

              @order_ids = Order.where(:status=>"In progress...").order("created_at desc")

            elsif option == Order.statuses[:delivered]

              @order_ids = Order.where(:status=>"Delivered").order("created_at desc")

            elsif option == Order.statuses[:cancelled]
              
              @order_ids = Order.where(:status=>"Cancelled").order("created_at desc")

            else

              @order_ids = Order.where(:status=>"Unable to delivered").order("created_at desc")
            end
          
        end
         @orders = Order.get_all_pending_orders(option)
      
      end

  end

  # print order invoice 
  def gen_order_invoice
    
      @order = Order.find(params[:id])
      @order_items = Order.get_only_order(current_user, @order)

      render json: { order: @order, order_items: @order_items, status: GlobalConstants::SUCCESS }

  end

  private

    def find_all_order_ids

    end

    def product_avaliability(quantity, product_id)

      product = Product.find(product_id)

      orders_items = OrderItem.where(:product_id => product.id)
       orders_no = orders_items.to_a.sum { |e| e.quantity }
    
      if ((product.quantity - orders_no) >= quantity.to_i)
        
          return true
      else
           return false
      end 

    end

    def hash_of_items(params)
      
        @items = []
        @total_amount_to_paid = 0

        if params[:product_id].nil? || params[:product_id].to_i == 0

          cart_ids = params[:ids]

          cart_ids.each do |id|

            cart_item = CartItem.find(id)

            item = Product.find( cart_item.product_id )

            @items.push(:name => item.name,
                  :sku => "SKU" + item.id.to_s,
                  :price => "%.2f" % item.price,
                  :currency => "USD",
                  :quantity => cart_item.quantity.to_s)

            @total_amount_to_paid += cart_item.quantity * item.price

          end

        else

          item = Product.find(params[:product_id])
          
          @items.push(:name => item.name,
                  :sku => "SKU" + item.id.to_s,
                  :price => "%.2f" % item.price,
                  :currency => "USD",
                  :quantity => params[:quantity].to_s)

          @total_amount_to_paid += params[:quantity].to_i * item.price

        end

        return @items, @total_amount_to_paid

    end
      
    def validate_card
      unless credit_card.valid?
        credit_card.errors.full_messages.each do |message|
          errors.add_to_base message
        end
      end
    end
    
    def credit_card(params)
     
      @credit_card ={:type => params[:card_type],
            :number => params[:payee][:card_number],
            :expire_month => params[:date]["card_expires_on(2i)"],
            :expire_year =>  params[:date]["card_expires_on(1i)"],
            :cvv2 => params[:payee][:card_verification],
            :first_name => params[:payee][:first_name],
            :last_name => params[:payee][:last_name],
            :billing_address => {
              :line1 => current_user.street_line1 + "," + current_user.street_line2 ,
              :city => current_user.city,
              :state => current_user.state,
              :postal_code => current_user.pin,
              :country_code => "IN"
            }
        }

      return @credit_card

    end
  
end
