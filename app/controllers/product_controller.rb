class ProductController < ApplicationController

  #check user authetication to restrict user 
  #to create , edit and update a product
  before_action :authenticate_user!, only: [:new, :edit,:create, :update]
  #check wether user is in admin role or not to restrict some functionality
  before_action :check_for_admin, only: [:new, :edit,:create, :update]

  #interface to create new product
  def new
  	@product = Product.new
    #retrive product categories
  	get_product_type
  end

  #action to return products
  def index
    get_product_type
    @product = Product.new

    #check wether any category selected or not
    if(params[:filter].nil? || params[:filter].blank? )
         # get products with search or without search 
         @products = Product.get_searched_product(params)
                                 .paginate(:page => params[:page], :per_page => 16)    	

    #checking if selected category is exists or not
    elsif ProductsType.exists?(:id => params[:filter])      
      @products = filter.paginate(:page => params[:page], :per_page => 16)
    else
      flash[:info] = "Product category doesn't exists"
      redirect_to product_index_path
    end

  end

  #action to return details of one product to view
  def show

  	@product = Product.find(params[:id])
    if @product.isVisible_to_user || current_user.isAdmin

      #get all reviews given by users for selected product
      @reviews = Product.get_reviews_for_a_product(params)

      #get all products related to current selected product
      obj_related_products = RelatedProduct.get_all_related_products(@product.id)

      if (!obj_related_products.nil?)

        str_related_products = obj_related_products.related_products

        @related_products = Product.get_all_related_products_details( str_related_products )
        
      else
        @related_products = obj_related_products
      end

      directory =GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + @product.id.to_s

      @additional_product_images = nil

      if Dir.exists?(directory)
        @additional_product_images = Dir.foreach(directory ).
                                      select{ |x| File.file?("#{GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + @product.id.to_s}/#{x}")}

      end
    else
      flash[:danger]="Product does not exist"
      redirect_to root_path
    end
    
    rescue ActiveRecord::RecordNotFound => e
    flash[:warning] = "Sorry product not found !"
    redirect_to root_path

  end


  def edit
  	get_product_type
  	@product = Product.find(params[:id])

  end

  #action to search a product 
  #here this method redirect to index action where search product will  return
  def search
  	redirect_to product_index_path(:search => params[:search])
  end

  #create new product
  def create

    get_product_type
    @product = Product.new(product_params) 
    @product.likes=0
    @product.unlikes=0
    # if image is selected then save the image
    if (!params[:product][:image].nil?)
      uploaded_io = params[:product][:image]
      #method to save image file
      save_image(uploaded_io)   	
      #update the current product instace image variable data
      @product.image = (( Product.last.present? ? Product.last.id : 0) + 1).to_s+"_"+uploaded_io.original_filename
    end    
    #insert new product into database server
    if @product.save
      Picture.create_new(@product)
      create_products_location_relation(@product)
    	flash[:success] = "Product added successfully"  	
    	redirect_to product_index_path

    elsif @product.errors.count>0
        render "new"
      else
      	flash[:danger] = "failed to save"
      	redirect_to new_product_path
    end

  end

  #action to update a product 
  def update
    @product = Product.find(params[:id])
    #block to delete image if another image is seleted to replace the existing one
    if (params[:product][:image] && !@product.image.nil? && (File.exist?(Rails.root.join('app/assets/images', 'products', @product.image))))
            File.delete(Rails.root.join(image_path, 'products', @product.image))
    end
    #block to update product details
    if @product.update_attributes(product_params)
      #update selected cities
      create_products_location_relation(@product)
      if( !params[:product][:image].nil? )          
          uploaded_io = params[:product][:image]
          #save new image
          save_image(uploaded_io)
          #update image name
          @product.update_attribute(:image, (Product.last.id+1).to_s+"_"+uploaded_io.original_filename)          
      end
      flash[:success] = "updated successfully"
      redirect_to product_path
    else

      flash[:danger] = "failed to update"
      redirect_to edit_product_path(@product)
    end

  end

   #related product of a product 
   def products_by_category
     @products = Product.get_all_products_by_types(params)
     @related_products = RelatedProduct.find_by(:product_id=>params[:id])
     render json: {:products => @products, :related_products => @related_products}
   end
   
  #return all product categories
  def get_product_type
  	@ProductsType = ProductsType.all.order(:name)
  end

  #method to create new category
  def add_category

    @product_type = ProductsType.new(:name => params[:cat_name])
    @product_type.save
    render json: {:product_type => @product_type}

  end

  #create related product record if not exist or update
  def create_update_related_product

     if(RelatedProduct.create_update_related_product(params))
      render json: {:id =>params[:id], :status => "success"}
     end
  end

  #check wether seleted city is deliverable or not
  def check_product_for_city
    @selected_cities = Product.select("area_to_available")
                  .where("id=#{params[:product_id]}")[0]

    unless @selected_cities.area_to_available.nil?
      if @selected_cities.area_to_available.downcase.split(",").include?(params[:city].downcase)
        render json: {:status => "available"}
      else
        render json: {:status => "unavailable"}
      end
    else
      render json: {:status => "unavailable"}
    end
    
  end

def count_additional_images
    images_count = Dir[GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + params[:id].to_s+"/**/*"].count { |file| File.file?(file) }    
    render json: { :status => GlobalConstants::SUCCESS ,:images_count => images_count}
end

def get_additional_images
  
    directory =GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + params[:id].to_s
    @additional_product_images = nil
    if Dir.exists?(directory)
      @additional_product_images = Dir.foreach(directory ).
                                    select{ |x| File.file?("#{GlobalConstants::PRODUCT_ADDITIONAL_IMAGE_PATH + params[:id].to_s}/#{x}")}
    end
    render json: { :status => GlobalConstants::SUCCESS, :image_name => @additional_product_images, :product_id => params[:id] }
end

#all the private method start from here
private 
  
  def create_products_location_relation(product)    
    product.locations.destroy_all
    unless params[:selected_cities].nil?
        params[:selected_cities].each do |city_id|
        city = Location.find(city_id.to_i)
        product.locations << city
     end
    end
  end

  #save selected image
  def save_image(uploaded_io)
    #file open
    File.open(Rails.root.join(image_path, 'products', 
              ( (Product.last.present? ? Product.last.id : 0 ) + 1 ).to_s+"_"+uploaded_io.original_filename), 'wb') do |file|    
    #write coverted bytes to file
    file.write(uploaded_io.read)
    end  
  end

  #all the fields to restricted to access 
	def product_params
    puts params
		params.require(:product).permit(:name, 
                                    :image,
                                    :area_to_available, 
                                    :product_type_id,
                                    :price, 
                                    :description, 
                                    :isVisible_to_user,
                                    :quantity)
	end

  #returns current user role
	def check_for_admin
		unless current_user.isAdmin? 
			flash[:warning] = "Access denied"	
			redirect_to root_path
		end
	end

  # filter the product as per selection
  def filter
    Product.filter_product(params[:filter])
  end
end
