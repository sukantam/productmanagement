class ReviewsController < ApplicationController
	before_action :authenticate_user!

	def create
		@review = Review.create_review(params, current_user)
		if(@review)
		
		render json: {:user => current_user,
					  :review => @review,
					  :created_at=> @review.created_at.to_formatted_s(:long_ordinal) }
		end
	end
end
