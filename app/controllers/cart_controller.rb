class CartController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found_wrong_access
  before_action :authenticate_user!, only: [:create, :index]

  #return all products added to cart for logged in user
  def index

    #return all products added to current logged in user carts to display in browser
  	@carts_product = Cart.get_all_products_in_user_cart(current_user)

    #calculate total amount
  	@total_amount = 0
    @total_amount = @carts_product.to_a.sum { |e| e.price.to_f * e.quantity }

    if @carts_product.present?
      #pagination
      @carts_product = @carts_product.paginate(:page => params[:page], :per_page => 12)
    end
  		
  end



  #remove cart item
  def destroy

    CartItem.find(params[:id]).destroy
    render :index

  end

  def quantity_update

    #method to update quantity
    Cart.quantity_update(params)   
    render nothing: true   

  end
end
