class ConversationsController < ApplicationController

	before_filter :authenticate_user!

	layout false

	def create
		if Conversation.between(params[:sender_id], params[:recipient_id]).present?
			@conversation = Conversation.between(params[:sender_id], params[:recipient_id]).first
		else
			@conversation = Conversation.create!(conversation_params)
		end

		render json: { conversation_id: @conversation.id }
	end

	def show
		
		@conversation = Conversation.find(params[:id])
		@reciever = get_current_reciever(@conversation)
		@messages = @conversation.messages
		@message = Message.new

	end


	def notify_typing_action
		
		@conversation_id = params[:id]

		@conversation = Conversation.find(@conversation_id)

		@id = ( @conversation.sender_id == current_user.id ? @conversation.recipient_id : @conversation.sender_id )
		@message_text = params[:message]
		@key_pressed = params[:event_key]
		@path = conversation_path(@conversation)

	end

	private

	def conversation_params

		params.permit(:sender_id, :recipient_id)

	end

	def get_current_reciever(conversation)
		
		current_user == conversation.recipient ? conversation.sender : conversation.recipient

	end

end
