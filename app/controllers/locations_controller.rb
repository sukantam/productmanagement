class LocationsController < ApplicationController

  load_and_authorize_resource
  
  def index
  	@location = Location.new
  	@locations = Location.all.order("name asc")
  end

  def create

  	@location = Location.new(location_params)
  	respond_to do |format|
  		if @location.save
  			format.html { redirect_to @location, notice: "Successfully added" }
  			format.js {}
  			format.json { render json: @location, status: :created, location: @location }
  		else
  			format.html { render action: 'new' }
  			format.json { render json: @location.errors, status: :unprocessable_entity }
  		end
  	end
  end

 def destroy  
 	Location.find(params[:id]).destroy
 	render json: { id: params[:id]}
 end

 private
 def location_params
    params.require(:location).permit(:name)
 end
end
