class ProductsLikesController < ApplicationController
	before_action :authenticate_user!
	
	#update likes or unlikes againgst products for user specific
	def create
	   @product = Product.find(params[:product_id])
	   updatelikes_unlikes(@product)

	   #check if user is liked or unliked
	   @productlike = ProductLike.where(:product_id=>params[:product_id],
	   									:user_id => current_user.id)
	   if @productlike.count > 0
	   		if @productlike.update_attribute(:like => params[:type])
	   			respond_back(true)
	   		else
	   			respond_back(false)
	   		end
	   else
	   #create record for product likes table
	   @productlike = ProductLike.new(:product_id=>params[:product_id],:user_id=>current_user.id,:like => params[:type].to_i)
	  		 if @productlike.save
	  		 	respond_back(true)
	  		 else
	  		 	respond_back(false)
	  		 end
	   end
	end

	private
	def updatelikes_unlikes(product)
		if params[:type]=='1'
			product.update_attributes(:likes => product.likes+1)
		else
			product.update_attributes(:unlikes => product.unlikes+1)
		end
	end

	def respond_back(status)
		if status
			respond_to do |format|
   				 msg = { :status => "ok", :message => "Success!",:product_id => params[:product_id] }
                 format.json  { render :json => msg } 
  			end
	   else
	   		respond_to do |format|
    			msg = { :status => "ok", :message => "failed!" }
    			format.json  { render :json => msg } # don't do msg.to_json
 			 end
 		end
 	end
end