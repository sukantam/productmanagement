module OrderHelper
	def return_class_on_status(order)
		if order.status == "Delivered"
			"order-details-delivered"
		elsif order.status == "In progress..."
			"order-details-processing"
		elsif order.status == "Cancelled"
			"order-details-cancelled"
		else
			"order-details-unreachable"
		end
			
	end
end
