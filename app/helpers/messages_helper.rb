module MessagesHelper

	def self_or_other(message)
		
		message.user == current_user ? "self" : "other"

	end

	def message_interlocutor(message)
		message.user == message.conversation.sender ? message.conversation.sender : message.conversation.recipient
	end

	def return_class_on_user(message)
		message.user == current_user ? "my-conversation-with-other" : " "
	end

	def return_class_on_user_image(message)
		message.user == current_user ? "user-image-border-color-with-other" : " "
	end

end
