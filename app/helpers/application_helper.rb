module ApplicationHelper

	$cities = Location.all.order("name asc")
	$users = User.all
	
	def errors_for(model, attribute)
	  if model.errors[attribute].present?
	    content_tag :span, :class => 'error-explanation' do
	      model.errors[attribute].join(", ")
	    end
	  end
	end

	#Returns athe full title on a per-page basis.
	def full_title(page_title='')
		base_title="Product management"
		if page_title.empty?
			base_title
		else
			page_title 
		end
	end

	# return number of items in your cart
	def cart_counter
		counter = 0
		
		if (current_user.carts.first).cart_items.present?
			counter = (current_user.carts.first).cart_items.count.to_s
		end
	end

	def product_types
		product_types = ProductsType.all
	end

	#get shipping address

	def shipping_addresses
		@address = ShippingAddress.get_all_shipped_address(current_user)
	end
	
	def bootstrap_class_for(flash_type)
	    case flash_type
	      when "success"
	        "alert-success"
	      when "error"
	        "alert-error"
	      when "alert"
	        "alert-block"
	      when "notice"
	        "alert-info"
	      else
	        flash_type.to_s
	    end
  end
end
