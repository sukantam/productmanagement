module ProductHelper
	def isLiked(product_id)
		ProductLike.exists?(:product_id => product_id, :user_id => current_user.id, :like => 1)
	end
	def isUnliked(product_id)
		ProductLike.exists?(:product_id => product_id, :user_id => current_user.id, :like => 0)
	end
	def product_avalabiliity(product_id)
		product = Product.find(product_id)
		orders_items = OrderItem.where(:product_id => product_id)
		orders_no = orders_items.to_a.sum { |e| e.quantity }
		
		product.quantity - orders_no
		

	end
	def get_selected_cities(product_id)

		product = Product.find(product_id)
		@selected_cities = 	product.locations.pluck(:name)	
		
	end
end
