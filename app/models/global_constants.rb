module GlobalConstants

	PRODUCT_ADDITIONAL_IMAGE_PATH = "app/assets/images/products/additional_images/"
	USER_ADDITIONAL_IMAGE_PATH = "app/assets/images/users/additional_images/"
	PRODUCT_ADDITIONAL_IMAGE_DISPLAY_PATH = "/assets/products/additional_images/"

	SUCCESS = "success"
	FAILURE = "failed"

end