class Review < ActiveRecord::Base
	belongs_to :user
	belongs_to :product

	def Review.create_review(params, current_user)
		@review=Review.new(:product_id => params[:product_id],:user_id => current_user.id,
							 :comment => params[:comment], :likes => 0, :unlikes => 0)
		@review.save 
		return @review
	end
end
