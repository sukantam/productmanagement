class Cart < ActiveRecord::Base
has_many :cart_items

  #return all products added to current logged in user carts
  def Cart.get_all_products_in_user_cart(current_user)

	  	cart = current_user.carts.first
	  	if cart.present?
	  		cart_items = cart.cart_items
	  		if cart_items
	  			cart_items.joins(:product)
	  						.select("cart_items.*,products.name as name,
	  								products.image,products.price")	  		
	  		else
	  			nil
	  		end
	  	else
	  		nil
	  	end
  end

  #update no. of product of specific type
  def Cart.quantity_update(params)

  	 @cart = CartItem.find(params[:id])
    @cart.update_attribute(:quantity, params[:quantity])
    	
  end

  #add product to cart
  def Cart.create_cart(id)

  	@cart = Cart.new(user_id: id)  	
  	@cart.save
  end
end
