class OrderTransaction < ActiveRecord::Base

	belongs_to :order

	def OrderTransaction.create_transaction(response, order_id, amount)
		
		@order_transaction = OrderTransaction.new( 
												 :order_id => order_id,
												 :amount => amount, 
												 :success => true, 
												 :message => response.state,
												 :mode => response.payer.funding_instruments[0].credit_card.type,
												 :payment_id => response.id,
												 :transaction_id => response.transactions[0].related_resources[0].sale.id )
		@order_transaction.save

	end
end
