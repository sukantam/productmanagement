class ShippingAddress < ActiveRecord::Base
	belongs_to :user

	def ShippingAddress.create_ship_addr(user)
		address = user.street_line1 + " ," + user.street_line2 + " ," + user.city + " ," + 
				  user.state + ", " + user.pin + ", Contact no: " + user.phone + ", " + user.country 
		ship_addr = user.shipping_addresses.new(:address_to_ship => address, :isSelected => true)
		ship_addr.save
	end

	def ShippingAddress.get_all_shipped_address(current_user)
		current_user.shipping_addresses.all.order("created_at desc")
	end

	def ShippingAddress.created_new_address(current_user, address)
		current_user.shipping_addresses.update_all(isSelected: false)
		ship_new_addr = current_user.shipping_addresses.new(:address_to_ship=> address, :isSelected => true)
		ship_new_addr.save

	end

	def ShippingAddress.update_selected_address(current_user, address_id)
		current_user.shipping_addresses.update_all(isSelected: false)
		current_user.shipping_addresses.find(address_id).update_attribute("isSelected", true)

	end

	def ShippingAddress.remove(current_user, id)
		current_user.shipping_addresses.find(id).destroy
	end
end
