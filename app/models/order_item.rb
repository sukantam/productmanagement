class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product

  def OrderItem.get_all_items_of_order(order)
  		order.order_items.joins(:product,:order).
  				  select("order_items.*,products.name, products.image, orders.expected_devlivery_on,
                    orders.delivery_on, orders.status, orders.delivery_place").
  				  order("order_items.created_at desc")
  end

  def OrderItem.create_order_item(cart_item, order)
    
  	order_item = order.order_items.new(:product_id => cart_item.product_id,
  						:total_price => (cart_item.quantity * (Product.find(cart_item.product_id)).price), 
  						:quantity => cart_item.quantity)
  	order_item.save
    return order_item
  end
end
