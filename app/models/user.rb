class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :carts, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :product_likes, dependent: :destroy
  has_many :shipping_addresses, dependent: :destroy
  has_many :pictures, as: :imageable
  has_many :conversations, :foreign_key => :sender_id
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :fullname, presence:true, length: {maximum: 255}
  validates :phone,   :presence => true,
                     :numericality => true,
                     :length => { :minimum => 10, :maximum => 15 }
  validates :pin,   :presence => true,
                     :numericality => true,
                     :length => {:minimum => 6, :maximum => 6 }
 #  has_attached_file :image, :styles => { :original => "922x922>", :thumb => "220x220>" }, default_url: "/images/:style/missing.png"
 #  validates_presence_of :image
 #  validates_attachment :image, :presence => true,
 # :content_type => { :content_type => ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] }
end
