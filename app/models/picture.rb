class Picture < ActiveRecord::Base
	belongs_to :imageable, polymorphic: true

	def Picture.create_new(object)
		picture_new = object.pictures.new(:image => object.image)
		picture_new.save
	end
end
