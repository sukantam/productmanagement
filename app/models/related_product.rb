class RelatedProduct < ActiveRecord::Base
	belongs_to :product

	def RelatedProduct.get_all_related_products(product_id)
		
		RelatedProduct.find_by(:product_id => product_id)

	end

	def RelatedProduct.create_update_related_product(params)
		product = Product.find(params[:id].to_i)

		if RelatedProduct.exists?(:product_id => params[:id].to_i)
			related_product = RelatedProduct.find_by(:product_id=> params[:id].to_i)
			related_product.update_attribute("related_products", params[:ids].join(','))

		else			
			new_related_product = RelatedProduct.new(:related_products => params[:ids].join(','), :product_id => params[:id].to_i)
			new_related_product.save

		end

	end
end
