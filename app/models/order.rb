class Order < ActiveRecord::Base
	has_many :order_items
	has_many :order_transactions, dependent: :destroy

	def self.statuses
		{:pending => 1, :delivered => 2, :cancelled=> 3, :unreachable => 4}
	end

	def Order.get_all_placed_orders(current_user)

		@orders_item = Array.new 
		orders = current_user.orders.order("created_at desc")

		orders.each do |order|
			@orders_item.push(OrderItem.get_all_items_of_order(order))

		end
		return @orders_item
	end

	#payment transaction precess
	def Order.pay_now(params, current_user, item_list, credit_card)
		
		require 'paypal-sdk-rest'
		include PayPal::SDK::REST
		PayPal::SDK::REST.set_config(
		  :mode => "sandbox", # "sandbox" or "live"
		  :client_id => "AVO8EAvbySmJk8fDak58e4qeKfFRT28Ylb2g2eq2DVxnDCUhGmRkJ_DbzHXOHvMza-5WbYUGmELQSMf-",
		  :client_secret => "EDpAXhFOHEXEr_SnW5bgfuGQmDuA3sliI6RIMZI3FEd6BcK30L64tVnffchb0GjkeLq0T180t2Onz1yy")
		
		#include PayPal::SDK::REST		
		#PayPal::SDK::Core::Config.load('config/paypal.yml',  ENV['RACK_ENV'] || 'development')
		@payment =Payment.new({
			:intent => "sale",
			:payer => {
				:payment_method => "credit_card",
			    :funding_instruments => [{
			      :credit_card => credit_card }]},
		    :transactions => [{
				   :item_list => {
				      :items => item_list[0] },
				    :amount => {
				      :total => "%.2f" % item_list[1],
				      :currency => "USD" },
				    :description => "This is the payment transaction description." }]      
			})

			if @payment.create
			  @payment     # Payment Id
			else
			  @payment  # Error Hash
			end

		return @payment
	end

	def Order.get_only_order(current_user, order)		
		OrderItem.get_all_items_of_order(order)
	end


	def Order.create_order(current_user)
		@order = current_user.orders.new(:expected_devlivery_on => Date.today + 3.days, :status => "In progress...")
		@order.save
		return @order
	end

	def Order.get_all_pending_orders(option)
		@orders_item = Array.new
		if option == self.statuses[:pending]
			orders = Order.select("orders.*").where(:status=>"In progress...").order("created_at desc")
			orders.each do |order|
				@orders_item.push(OrderItem.get_all_items_of_order(order))
			end
		elsif option == self.statuses[:delivered]
			orders = Order.select("orders.*").where(:status=>"Delivered").order("created_at desc")
			orders.each do |order|
				@orders_item.push(OrderItem.get_all_items_of_order(order))
			end
		elsif option == self.statuses[:cancelled]
			orders = Order.select("orders.*").where(:status=>"Cancelled").order("created_at desc")
			orders.each do |order|
				@orders_item.push(OrderItem.get_all_items_of_order(order))
			end
		else
			orders = Order.select("orders.*").where(:status=>"Unable to delivered").order("created_at desc")
			orders.each do |order|
				@orders_item.push(OrderItem.get_all_items_of_order(order))
			end
		end
		return @orders_item
	end

	# get ids of all matched orders
	def Order.get_orders_id(params) 

		order_id = params[:search].scan(/\d+/).last
		status = ""

		if params[:filter].to_i == self.statuses[:pending]
			status = "In progress..."
		elsif params[:filter].to_i == self.statuses[:delivered]
			status = "Delivered"
		elsif params[:filter].to_i == self.statuses[:cancelled]
			status = "Cancelled"
		else
			status = "Unable to delivered"			
		end

		Order.where('orders.status=? and orders.id = ?',status,order_id).order("created_at desc")

	end

	#return all matched orders
	def Order.get_all_matched_orders(order_ids)
	
		@orders_item = Array.new
		order_ids.each do |order|
			@orders_item.push(OrderItem.get_all_items_of_order(order))
		end				
		return @orders_item
	end

	def Order.update_status(params)
		order = Order.find(params[:order_id])
		if params[:type] == "Delivered" || params[:type] == "Cancelled"
			order.update_attributes(:status => params[:type], :delivery_on => Date.today)
		else
			order.update_attribute("status", "Unable to delivered")
		end
	end

	
end
