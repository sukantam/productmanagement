class CartItem < ActiveRecord::Base
	belongs_to :product
	belongs_to :cart

	def CartItem.create_item(params,current_user)
		cart = current_user.carts.first
		
		cart_item = cart.cart_items.new(quantity: params[:cart][:quantity], product_id: params[:product_id])
		cart_item.save
	end
	
end
