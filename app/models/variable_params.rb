def sample(*args)
	puts "The number of parameters are #{args.length}"

	for i in 0...args.length 
		puts "The parameter #{i+1} is #{args[i]}"
	end
end