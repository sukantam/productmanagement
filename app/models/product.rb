class Product < ActiveRecord::Base
	has_many :orders
	has_many :cart_items
	has_many :reviews, dependent: :destroy
	belongs_to :products_like
	has_one :related_product
	has_many :pictures, as: :imageable
	has_and_belongs_to_many :locations
	validates_presence_of :product_type_id, message: "Select category"
	validates :name, :presence =>true
	validates :price, :presence => true, :numericality => true
	validates :image, :presence => true
	validates :description, :presence => true
	validates :quantity, :presence => true, :numericality => {:only_integer => true}

	# has_attached_file :image, :styles => { :original => "922x922>", :thumb => "220x220>" }, default_url: "/images/:style/missing.png"
 #  	validates_presence_of :image
 #  	validates_attachment :image, :presence => true,
 # 						 :content_type => { :content_type => ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] }
	
	#get all products of specific category type

	def Product.get_all_products_by_types(params)
		Product.all.where("product_type_id = #{params[:type_id]}")	
	end

	#get product on product category selection basis
	def Product.filter_product(selected_category)
		unless selected_category.nil?
			@products = Product.where(:product_type_id => selected_category).order("created_at desc")

		end
	end

	#get all name matched products 
	def Product.get_searched_product(params)

		if params[:search].nil?
      		Product.all.order("created_at desc")
      	else
      		Product.where('name LIKE ?', "%#{params[:search]}%")
                             .order("created_at desc")
      	end
	end

	#return all reviews given by users for selected product
	def Product.get_reviews_for_a_product(params)

		Review.joins("left join users on reviews.user_id= users.id")
            .where("reviews.product_id=?",params[:id])            
            .select("reviews.*,users.id,users.fullname")            
            .order("reviews.created_at desc")
            
	end

	def Product.get_all_related_products_details(str_related_products)
		temp_ids = str_related_products.split(",").map(&:to_i)
		
		Product.where("id in (?)",temp_ids)
	end
end
