var ready= function() {
   $("body").on("click",".btn-order-status-cancelled", function(){
    $("#wait").css("display", "block");
    event.preventDefault()

    order_id = $(this).attr('id').split('_')[2];
    if(order_id){
      $.ajax({
        type:'PUT',
        url:"/update_order_status",
        data:{order_id : order_id, type : "Cancelled"},
        success:function(data){
          $("#wait").css("display", "none");
          
          $('#spn_show_order_status_'+ data.order_id).text("Cancelled");
          $('#btn_cancelled_'+data.order_id).css("display","none");
          $('#parent_row_'+data.order_id).addClass("order-details-cancelled");
        }
      });
    }
  });
  $(".btn-order-status-delivered").on("click", function(){
    $("#wait").css("display", "block");
  	event.preventDefault();
  	order_id = $(this).attr('id').split('_')[2];

      if(order_id){
        $.ajax({
          type:'PUT',
          url:"/update_order_status",
          data: { order_id : order_id, type : "Delivered" },
          success:function(data){
            $("#wait").css("display", "none");
          
            //I assume you want to do something on controller action execution success         
            console.log(data);
            $("#parent_row_"+data.order_id).hide('slow', function(){ $("#parent_row_"+data.order_id).remove(); });
            $("#child_row_"+data.order_id).hide('slow', function(){ $("#parent_row_"+data.order_id).remove(); });
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            unauthorize(errorThrown);
          }    
        });  
      }
  });

  $(".btn-order-status-unreachable").on("click", function(){
    $("#wait").css("display", "block");
  	event.preventDefault();
  	order_id = $(this).attr('id').split('_')[2];
      if(order_id){
        $.ajax({
          type:'PUT',
          url:"/update_order_status",
          data: { order_id : order_id, type : "unreachable" },
          success:function(data){
            $("#wait").css("display", "none");
          
            //I assume you want to do something on controller action execution success         
            console.log(data);
            $("#parent_row_"+data.order_id).hide('slow', function(){ $("#parent_row_"+data.order_id).remove(); });
            $("#child_row_"+data.order_id).hide('slow', function(){ $("#parent_row_"+data.order_id).remove(); });
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            unauthorize(errorThrown);
          }    
        });  
      }
  });

  $(".link-order-invoice-gen").on("click", function(){
    $("#wait").css("display", "block");
    event.preventDefault();

    order_id = $(this).attr('id').split('_')[4];
      if(order_id){
        $.ajax({
          type:'GET',
          url:"/order/"+order_id+"/gen_order_invoice",
          data: { },
          success:function(data){
            $("#wait").css("display", "none");
          
            //I assume you want to do something on controller action execution success         
            console.log(data);

             print_order(data);

          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            unauthorize(errorThrown);
          }    
        });  
      }

  });

  function print_order(data){

    html_format = '<table style="width:100%;">';
    html_format = html_format + '<tr class="order-details-header", style="width: 100%;background-color: #131212;color: #fff;height: 55px;" >'+
      '<td colspan="4", style="width:100%;">'+
      '<div style="width:33%; float:left;">ORDER-ID: ORD#' +data.order.id+ '</div>' +
      '<div style="width:33%;float:left;">ORDER ON: '+data.order.created_at+ '</div>'+
      '<div style="width:33%;float: right;  text-align: center;"> Rs.' + data.order.paid_price
      '</div></td></tr>'+
      '<tr><td>'+
      '<div style="border-style:solid; border_width:1px;">'+
      '<table>';

      for(i =0; i<data.order_items.length; i++){
        html_format = html_format + ' <tr >'+
            '<td style="border-style: solid;  border-width: 1px;">'+(i+1)+'</td>'+     
              '<td style="border-style: solid;  border-width: 1px;"> '+ data.order_items[i].name + '</td>'+
              '<td style="border-style: solid;  border-width: 1px;">Rs.'+data.order_items[i].total_price+'</td>'+
              '<td style="border-style: solid;  border-width: 1px;">Qnt: '+data.order_items[i].quantity+'</td>'+
            '</tr>';          
      }

      html_format = html_format + '</table></div>'+
          '<div class="order-status-section">'+
            '<table style="width: 100%;background-color: #131212;color: #fff;height: 55px;">'+
              '<tr>'+
                '<td>Expected on : <span>'+data.order.expected_devlivery_on+'</span> </td>'+
                '<td>Delivered on : <span>'+data.order.delivery_on +' </span> </td>'+
                '<td>Status : <span>'+data.order.status +' </span></td>'+
              '</tr>'+
              '<tr>'+
                '<td>Delivered address : </td>'+
                '<td colspan="2">'+data.order.delivery_place+'</td>'+
              '</tr></table></div></td>'+
      '</tr>';
    html_format = html_format + '</table>';
    var printWindow = window.open('', '', 'height=800,width=800');
    console.log(printWindow);
            printWindow.document.write('<html><head><title>Order invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(html_format);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
  }

}
$(document).ready(ready);
$(document).on('page:load', ready);


