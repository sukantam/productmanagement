
var ready = function(){

$('body').on('click','.remove-location', function(){
	returnValue = confirm("Are you sure to remove ?");
	if(returnValue)
	{
		id = $(this).attr('id').split('_')[1];
		  if(id){
		      $.ajax({
		        type:'DELETE',
		        url:"/locations/"+id,
		        data: { },
		        success:function(data){
		        	$("#wait").css("display", "none");
          
		          //I assume you want to do something on controller action execution success 
		        $('#remove_'+data.id).closest('li').remove();
		        }
		      });      
		    }
		  }
	});
}
$(document).ready(ready);
$(document).on('page:load', ready);