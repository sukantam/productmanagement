// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require chat
//= require private_pub
//= require turbolinks
//= require bootstrap
//= require jquery.purr
//= require best_in_place
//= require ckeditor-jquery
//= require jquery-ui-1.10.3.custom.min.js
//= require jquery.mousewheel.min.js
//= require jquery.kinetic.min.js
//= require jquery.smoothdivscroll-1.3-min.js
//= require jquery.validationEngine-en.js
//= require jquery.validationEngine.js
//= require autocomplete-rails
//= require list
//= require_tree .


$(document).on("click",".change-role",function(){
  $(".overlay-role").show();
      $(".role-change-popup").show();
});

$(document).on("click",".up-down-img", function(){
	id = $(this).attr('id').split('_')[1]
	if($('#section_'+id).css('display') == 'none')
	{
		$('#section_'+id).css('display','block');
		$(this).attr('src','/assets/up-arrow-circle.png');
	}
	else
	{
		$('#section_'+id).css('display','none');
		$(this).attr('src','/assets/down_arrow_circle.png');
	}
});

var ready=function(){

	
}
$(document).ready(ready);
$(document).on('page:load', ready);