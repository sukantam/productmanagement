$(document).on("click",".edit-quantity",function(){
  $(this).closest('tr').find('td:eq(4)').find('input').prop('disabled', false);
  $(this).closest('tr').find('td:eq(4)').find('.save-quantity').show();
  $(this).closest('tr').find('td:eq(4)').find('.cancel-quantity').show();
  $(this).hide();
});
$(document).on("click",".cancel-quantity",function(){
  $(this).closest('tr').find('td:eq(4)').find('input').prop('disabled', true);
  $(this).closest('tr').find('td:eq(4)').find('.save-quantity').hide();
  $(this).closest('tr').find('td:eq(4)').find('.edit-quantity').show();
  $(this).hide();
});


var ready=function(){

  jQuery("#cart_form").validationEngine();

  $("body").on("click", ".save-quantity", function(){
    $("#wait").css("display", "block");
    cart_id = $(this).attr('id').split("_")[1];
    quantity = $("#field_"+cart_id).val();
    if(quantity){
      $.ajax({
        type:'PUT',
        url:"/cart/"+cart_id+"/quantity_update",
        data: { quantity : quantity },
        success:function(){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
        }
      });      
    }
          $("#field_"+cart_id).attr('disabled',true);
          $(this).hide();
          $(this).closest('tr').find('td:eq(4)').find('.edit-quantity').show();
          $(this).closest('tr').find('td:eq(4)').find('.cancel-quantity').hide();
  });

  $('#plc_cart_orders').click(function()
  {   
        event.preventDefault();
        $("#payment_modal").modal();
        $("#payment_modal .modal-dialog").css("width","80%");
  
  });

  $('body').on('click','.lnk-add-new-addr',function(){
    $('#txt_area_new_address').show();
  });
  $('body').on('click','#btn_close', function(){
    event.preventDefault();
    $('#address_overlay').hide();
    $('.address-section').hide();
    $('#txt_area_new_address').hide();
    $('#txt_area_new_address').val("");
    return false;
  });

  $('body').on('click','#link_check_out', function(){
     var valid = $("#cart_form").validationEngine('validate');
     var vars = $("#cart_form").serialize();
     if(valid == true ){
        if($('#txt_area_new_address').val()!='')
        {
          save_new_address($('#txt_area_new_address').val());
        }
        else
        {
          selected_option=''
           selected_option = address_selected();
           if(selected_option!="")
           {
              update_selected_address(selected_option);
           }
           else
              alert("Select address to ship");   
        }
    }
    else
    {
       $("#cart_form").validationEngine();
    }
  });

$('body').on('click','.remove-address', function(){
  $("#wait").css("display", "block");
  id = $(this).attr('id').split('_')[1];
  if(id){
      $.ajax({
        type:'DELETE',
        url:"/shipping_addresses/"+id,
        data: { },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success 
          $('#shipping_address_'+id).closest('div').remove() 
        }
      });      
    }
})

function update_selected_address(selected_option)
{
	console.log(selected_option)
if(selected_option){
      $.ajax({
        type:'PUT',
        url:"/shipping_addresses/"+selected_option+"/update_selected_address",
        data: { },
        success:function(data){
          //I assume you want to do something on controller action execution success 
        $('#address_overlay').hide();
        $('.address-section').hide();
        $('#cart_form').submit();       
        }
      });      
    }
}
  function save_new_address(address)
  {
  if(address){
      $.ajax({
        type:'POST',
        url:"/shipping_addresses",
        data: { address : address },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success 
        $('#address_overlay').hide();
        $('.address-section').hide();
        $('#cart_form').submit();       
        }
      });      
    }
    else
    {
      alert("Select or add new address");
    }
   
  }

  function address_selected()
  {
    var selectedVal = "";
    var selected = $(".address-each-section input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    }
    console.log(selectedVal)
    return selectedVal;
  }
}
$(document).ready(ready);
$(document).on('page:load', ready);