
var ready= function() {

jQuery("#formID").validationEngine();

$(".del-additional-img").click(function(){
  $("#wait").css("display", "block");
  idArr = $(this).attr("id").split("_");
  product_id = idArr[1];
  image_name = idArr[2];
  if(product_id && image_name ){
      $.ajax({
        type:'PUT',
        url:"/remove_image_frm_dir",
        data: { product_id : product_id, image : image_name },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          console.log(data);
          if(data.status = "success")
          {
            element = $("#additional_img_"+data.product_id);
            element.closest('li').remove();
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      });  
    }
});

$(".img-show-in-large").click(function(){
  $("#product_main_image").attr("src", $(this).attr("src"))
});

$(".lnk-add-additional-images").click(function(){
  $("#wait").css("display", "block");
  if($("#additional_image").val()!="")
  {
  product_id = $(this).attr('id').split('_')[3];
  if(product_id ){
      $.ajax({
        type:'GET',
        url:"/product/"+product_id+"/count_additional_images",
        data: { },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          console.log(data);
          if(data.status = "success")
          {
            if(parseInt(data.images_count) < 4 )
            {
              $("#picture_form").submit();
            }
            else{
              $("#spn_error_display").html("Maximum 4 additional images can be added");
              $("#spn_error_display").show();
            }
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      });  
    }
  }
  else
  {
    alert("select image");
  }
});

$("#myBtn").click(function(){
        $("#myModal").modal();
    });

$("#slider_main_section").smoothDivScroll({
      mousewheelScrolling: "allDirections",
      manualContinuousScrolling: true,
      autoScrollingMode: "onStart"
    });

  $(".link-add-related-product").on('click', function(){

    $("#wait").css("display", "block");

    product_id = $(this).attr('id').split('_')[3];
    ids = get_selected_products();

    console.log(ids)

    if(product_id && ids){

      $.ajax({
        type:'POST',
        url:"/product/"+product_id+"/create_update_related_product",
        data: { ids : ids },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          console.log(data);
          if(data.status = "success")
          {
            $('.btn-secondary').click()
            window.location.replace('/product/'+data.id);
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      }); 

    }
  });

  $(".related-products-popup").on('click', function(){

    $("#wait").css("display", "block");

    event.preventDefault();

    type_id = $(this).attr('id').split('_')[2];
    id = $(this).attr('id').split('_')[3];

    if(type_id){

      $.ajax({
        type:'POST',
        url:"/product/"+ id +"/products_by_category",
        data: { type_id : type_id },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          show_products(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      });

    }
  });

 function get_selected_products()
 {
   parent_div = $(".related-product-display-section");
   var checkedValues =[];
     $('.related-product-display-section :checked').each(function() {
       checkedValues.push($(this).val());
     });

   return checkedValues;
 }

  $(".txt-product-quantity").on("change",function(){

    product_id = $(this).attr("id").split("_")[2];
    total_quantity = parseInt($("#product_in_stoke_"+product_id).text());

    if (total_quantity < parseInt($(this).val()) )
    {
        alert("maximum of "+total_quantity+" orders can be placed");
        $(this).val(total_quantity);
      }
  });
  

  $(".img-zoom").hover(function(){

    product_id = $(this).attr('id').split("_")[1];
      $("#vlb_zoom_"+product_id).show();

    });

  $(".vlb_zoom").mouseleave(function()
    {
      product_id = $(this).attr('id').split("_")[1];
      $(this).hide();
    });

     $("body").on("click", ".vlb_zoom", function(){

       var height = $(window).scrollTop();       
        product_id = $(this).attr('id').split("_")[2];
        image = $("#image_"+product_id).attr("src");

        $(".overlay").show();
        $(".image-large").attr("src",image);
        $(".image-large-div").css("top",(130+height)+"px");
        $(".image-large-div").show();
        
        show_additional_images_of_a_product(product_id);
        
     });

     function show_additional_images_of_a_product(product_id)
     {
      $("#wait").css("display", "block");

      if(product_id){

        $.ajax({
          type:'GET',
          url:"/product/"+product_id+"/get_additional_images",
          data: { },
          success:function(data){
            $("#wait").css("display", "none");
          
            //I assume you want to do something on controller action execution success         
            console.log(data)

            additional_images_html ="<ul>";

            if(data.image_name && data.image_name.length > 0){

              for(i=0;i<data.image_name.length;i++)
              {
                additional_images_html +='<li><img src="/assets/products/additional_images/' +
                                     data.product_id + '/' + data.image_name[i] + '", alt="image", class="img-show-in-large-in-index" />'+
                  '</li>';
              }
              console.log(additional_images_html)

            }
            
              $(".div-additional-image").html(additional_images_html);
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            unauthorize(errorThrown);
          }    
        });  

      }
        
     }

  $("body").on("click", ".img-show-in-large-in-index", function(){

    $(".image-large").attr("src",$(this).attr("src"));

  });

  $("body").on("click", ".comment_link", function(){

    $("#wait").css("display", "block");

    event.preventDefault();

    comment = $("#comment_text").val();
    product_id = $(this).attr('id').split("_")[1];

    if(comment){

      $.ajax({
        type:'POST',
        url:"/reviews",
        data: { comment : comment,product_id : product_id },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          comment_html = '<div class="comments_each"><div><span>On '+ data.created_at+'</span> by '+
                          '<a href="/user_accounts/'+data.user.id+'">'+data.user.fullname+'</a></div>' +
                          '<div class="comments-text">'+data.review.comment+'</div></div>';
          $(".comments-display-section").prepend(comment_html);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      });  

    }
    else
    {
       $("#comment_text").addClass("error-empty");
    }    
       
  });

product_id=0;
quantity ="";

$("body").on("click", ".link-place-order", function(){

        event.preventDefault();

        $("#payment_modal").modal();
        $("#payment_modal .modal-dialog").css("width","80%");

        product_id = $(this).attr('id').split("_")[1];    
        quantity = $("#product_quantity_"+product_id).val();
  });

$('body').on('click','.lnk-add-new-addr',function(){

    $('#txt_area_new_address').show();

  });

  $('body').on('click','#btn_close', function(){

    event.preventDefault();

    $('#address_overlay').hide();
    $('.address-section').hide();
    $('#txt_area_new_address').hide();
    $('#txt_area_new_address').val("");

    return false;
  });

$('body').on('click','#link_product_check_out', function(){

     var valid = $("#formID").validationEngine('validate');
     var vars = $("#formID").serialize();

     if(valid == true ){

        if($('#txt_area_new_address').val()!='')
        {
          save_new_product_address($('#txt_area_new_address').val());
        }
        else
        {
          selected_option=''
           selected_option = address_selected();
           if(selected_option!="")
           {
              update_product_selected_address(selected_option);
           }
           else
              alert("Select address to ship");   
        }
    }
    else
      $("#formID").validationEngine();

  });

function update_product_selected_address(selected_option)
{
if(selected_option){
      $.ajax({
        type:'PUT',
        url:"/shipping_addresses/"+selected_option+"/update_selected_address",
        data: { },
        success:function(data){
          //I assume you want to do something on controller action execution success 
        $('#address_overlay').hide();
        $('.address-section').hide();
        place_order()       ;
        }
      });      
    }
}

function place_order()
{
    var fn = $("#payee_first_name").val();
    var ln = $("#payee_last_name").val();
    var ct = $("#card_type").val();
    var cn = $("#payee_card_number").val();
    var cvv = $("#payee_card_verification").val();
    var yr = $("#date_card_expires_on_1i").val();
    var mn = $("#date_card_expires_on_2i").val();

    var buyer = {first_name : fn, last_name : ln, card_number : cn, card_verification : cvv };
    var date = {"card_expires_on(2i)" : mn, "card_expires_on(1i)" : yr} ;
    console.log(buyer + " " + date);


    if(product_id && quantity){

      $("#wait").css("display", "block");

      $.ajax({
        type:'POST',
        url:"/product/"+product_id+"/order",
        data: { product_id : product_id, quantity : quantity, payee : buyer, date : date, card_type : ct },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          if (data.message !="failed")
          {
            $(".overlay").show();
            $(".order-placed").show();
          }
          else
          {
            alert("less order in stoke");
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          unauthorize(errorThrown);
        }    
      });  
    }
     
       
}

  function save_new_product_address(address)
  {
  if(address){
    $("#wait").css("display", "block");

      $.ajax({
        type:'POST',
        url:"/shipping_addresses",
        data: { address : address },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success 
        $('#address_overlay').hide();
        $('.address-section').hide();
        place_order();
        },
         error: function(XMLHttpRequest, textStatus, errorThrown) { 
            unauthorize(errorThrown);
         }
      });      
    }
    else
    {
      alert("Select or add new address");
    }
   
  }

  function show_products(data)
  {
    if(data.related_products!=null)
    {
      related_products = data.related_products.related_products.split(',');
      console.log(related_products)
    }

    html_format = '<ul class="users related-products-ul">';
    for (var i = 0; i <= data.products.length -1; i++) {
      html_format += '<li>'+
        '<a class="vlightbox2"><img src="/assets/products/'+ data.products[i].image+'", alt: "image",id: "image_'+ data.products[i].id+'}",class: "img-zoom") %>'+
        '</a>'+
        '<div><a href="/product/'+ data.products[i].id+'", title="'+ data.products[i].name +'">'+data.products[i].name.substring(0,21)+'...</a></div>'+
        '<div>Rs.'+ data.products[i].price +'</div>'+
        '<div>';

        if( data.related_products!=null && ($.grep(related_products, function(e){ return e == data.products[i].id.toString(); })).length>0)
           html_format += '<input type="checkbox", checked="checked", id="checked_'+ data.products[i].id +'", value="'+ data.products[i].id +'" /> ';
        else
          html_format += '<input type="checkbox", id="checked_'+ data.products[i].id +'", value="'+ data.products[i].id +'" /> ';
    }
    html_format += '</ul>';
    
    $(".overlay").show();
    $(".related-product-display-section").html(html_format);
    $("#show-all-product-type").show();
    
  }

$('.btn-secondary').on('click', function(){
  event.preventDefault();
  $(".overlay").hide();
  $(".related-product-display-section").html(html_format);
  $("#show-all-product-type").hide();
})


  function address_selected()
  {
    var selectedVal = "";
    var selected = $(".address-each-section input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    }
    return selectedVal;
  }

  $("body").on("click","#new-category", function(){
      $(".overlay").show();
      $(".show-category-panel").show();
  });

  $("body").on("click","#close_btn", function(){
      $(".overlay").hide();
      $(".show-category-panel").hide();
  });

  $("body").on("click","#add_category", function(){
        cat_name = $("#txt_category").val();

        if(cat_name){

          $("#wait").css("display", "block");

          $.ajax({
            type:'PUT',
            url:"/product/0/add_category",
            data: { cat_name : cat_name },
            success:function(data){
              $("#wait").css("display", "none");
          
              //I assume you want to do something on controller action execution success         
              $("#product_product_type_id").append('<option value='+data.product_type.id+'>'
                + data.product_type.name+'</option>');
              $(".overlay").hide();
              $(".show-category-panel").hide();
            }
          });  
        }
        else
        {
          alert("no blank");
        }  
  });

$("body").on("click",".unlike-btn", function(){

        product_id = $(this).attr('id').split("_")[1];  

        if(product_id){

          $("#wait").css("display", "block");

          $.ajax({
            type:'POST',
            url:"/products_likes",
            data: { product_id : product_id, type : 0 },
            success:function(data){  
              $("#wait").css("display", "none");
          
              $("#unlike_"+data.product_id).attr("src","/assets/right.png");
              $("#unlike_"+data.product_id).removeClass("unlike-btn");
              $("#unlike_"+data.product_id).addClass("unliked-btn");
              $("#like_"+data.product_id).removeClass("like-btn");
              $("#like_"+data.product_id).addClass("liked-btn");
              unlikes = $("#unlike_span").text();
              $("#unlike_span").text((parseInt( unlikes.split(" ")[0])+1)+" unlikes")
            },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
                unauthorize(errorThrown);
            }    
          });  
        }
        else
        {
          alert(data.message);
        }  
  });

$("body").on("click",".like-btn", function(){

    product_id = $(this).attr('id').split("_")[1];  

    if(product_id){

      $("#wait").css("display", "block");

      $.ajax({
        type:'POST',
        url:"/products_likes",
        data: { product_id : product_id, type : 1 },
        success:function(data){
          $("#wait").css("display", "none");
      
          $("#like_"+data.product_id).attr("src","/assets/right.png");
          $("#like_"+data.product_id).removeClass("like-btn");
          $("#like_"+data.product_id).addClass("liked-btn");
          $("#unlike_"+data.product_id).removeClass("unlike-btn");
          $("#unlike_"+data.product_id).addClass("unliked-btn");
          likes = $("#like_span").text();
          $("#like_span").text((parseInt( likes.split(" ")[0])+1)+" likes")
          
        },
      error: function(XMLHttpRequest, textStatus, errorThrown) { 
        unauthorize(errorThrown);
        }    
      });  
    }
    else
    {
      alert(data.message);
    }  
  });

 $("body").on("click", ".btn-product-availability", function(){

    event.preventDefault();

    city = $("#txt_product_avail").val();
    product_id = $(this).attr('id').split("_")[3];

    if(city && product_id){

      $("#wait").css("display", "block");
      
      $.ajax({
        type:'PUT',
        url:" /product/"+product_id+"/check_product_for_city",
        data: { city : city, product_id : product_id },
        success:function(data){
          $("#wait").css("display", "none");
          
          //I assume you want to do something on controller action execution success         
          if(data.status=="available")
          {
            $("#span_avail").text("Product is available for above city");
            $("#span_avail").attr("style","display:block;color:green;float: left;font-size: 12px;");
            $("#span_not_avail").attr("style","display:none;");
          }
          else
          {            
            $("#span_not_avail").text("Product is not available for above city");
            $("#span_not_avail").attr("style","display:block;color:red;float: left;font-size: 12px;");
            $("#span_avail").attr("style","display:none;");

          }
        }
      });  
    }
    else
    {
      $("#txt_product_avail").addClass("error-empty");
    }    
       
  });

 function unauthorize(status)
 {
    if(status=='Unauthorized') 
      {
        window.location.replace('/users/sign_in');
      }
        
 }
}

  
$(document).ready(ready);
$(document).on('page:load', ready);